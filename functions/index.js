/*
--== console commands ==--
npm install -g firebase-tools
firebase login
firebase init functions
firebase deploy --only functions
*/

//consts
const usersPath = "/users"
const partiesPath = "/parties"

//dependencies
const express = require('express');
const admin = require('firebase-admin');
const functions = require('firebase-functions');

//init
const app = express();
admin.initializeApp(functions.config().firebase);

//utils
let db = (table) => admin.database().ref(table)
let response = (res, val) => Promise.resolve(res.send(val))
let key = (val) => Object.keys(val).pop()
let keys = (val) => Object.keys(val)
let vals = (keys, data) => keys.map(i => data[i])

let handleSnapshotResults = (snap, res, onExists) => {
    if (snap.exists()) {
        onExists()
        return response(res, { status: 200 })
    }
    return response(res, {})
}

let returnSnapshotResults = (snap, res, result) => {
    if (snap.exists()) {
        return response(res, result())
    }
    return response(res, {})
}

//parties
app.post('/addParty', (req, res) => {
    return db(partiesPath)
        .push(req.body)
        .then(response(res, { status: 200 }))
});

app.get('/getParties', (req, res) => {
    return db(partiesPath)
        .once('value', snap => {
            return returnSnapshotResults(
                snap,
                res,
                () => vals(keys(snap.val()), snap.val())
            )
        })
});

//?id={id}
app.get('/getParty', (req, res) => {
    return db(partiesPath)
        .orderByChild('id')
        .equalTo(req.query.id)
        .once('value', snap => {
            return returnSnapshotResults(
                snap,
                res,
                () => vals(keys(snap.val()), snap.val()).pop()
            )
        })
});

//?id={id}
app.post('/deleteParty', (req, res) => {
    return db(partiesPath)
        .orderByChild('id')
        .equalTo(req.query.id)
        .once('value', snap =>
            handleSnapshotResults(
                snap,
                res,
                () => snap.child(key(snap.val())).ref.remove()
            )
        )
});

//?id={id}
app.post('/updateParty', (req, res) => {
    return db(partiesPath)
        .orderByChild('id')
        .equalTo(req.query.id)
        .once('value', snap =>
            handleSnapshotResults(
                snap,
                res,
                () => snap.child(key(snap.val())).ref.update(req.body)
            )
        )
});

//users
app.post('/addUser', (req, res) => {
    return db(usersPath)
        .push(req.body)
        .then(response(res, { status: 200 }))
});

app.get('/getUsers', (req, res) => {
    return db(usersPath)
        .once('value', snap => {
            return returnSnapshotResults(
                snap,
                res,
                () => vals(keys(snap.val()), snap.val())
            )
        })
});

//?id={id}
app.get('/getUser', (req, res) => {
    return db(usersPath)
        .orderByChild('id')
        .equalTo(req.query.id)
        .once('value', snap => {
            return returnSnapshotResults(
                snap,
                res,
                () => vals(keys(snap.val()), snap.val()).pop()
            )
        })
});

//?id={id}
app.post('/deleteUser', (req, res) => {
    return db(usersPath)
        .orderByChild('id')
        .equalTo(req.query.id)
        .once('value', snap =>
            handleSnapshotResults(
                snap,
                res,
                () => snap.child(key(snap.val())).ref.remove()
            )
        )
});

//?id={id}
app.post('/updateUser', (req, res) => {
    return db(usersPath)
        .orderByChild('id')
        .equalTo(req.query.id)
        .once('value', snap =>
            handleSnapshotResults(
                snap,
                res,
                () => snap.child(key(snap.val())).ref.update(req.body)
            )
        )
});

//export
exports.api = functions.https.onRequest(app)